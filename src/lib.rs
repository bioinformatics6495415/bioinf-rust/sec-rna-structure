use core::fmt::{self, Debug};
use std::usize;

use derive_more::Constructor;
use regex::Regex;
#[cfg(feature = "std")]
use std::error;
#[cfg(feature = "std")]
use std::io::{Error, ErrorKind};

#[derive(Debug, Constructor)]
pub struct NucleotidePair {
    /// Base 1 index for printing to Connectivity Table
    row_number: usize,
    nucleotide: char,
    pair_index: usize,
}

#[derive(Debug)]
pub struct BasePairSequence {
    pub description: String,
    pub secondary_structure: Vec<NucleotidePair>,
}

#[derive(Debug)]
pub struct FASTASequence {
    pub description: String,
    nucleotides: String,
    pub brackets: String,
    free_energy: f64,
}

macro_rules! assert_err {
    ($expression:expr, $($pattern:tt)+) => {
        match $expression {
            $($pattern)+ => (),
            ref e => panic!("expected `{}` but got `{:?}`", stringify!($($pattern)+), e),
        }
    }
}

/// This type represents all possible errors that can occur when serializing or
/// deserializing Sequence data.
#[derive(Debug)]
pub enum SequenceError {
    /// Wrapped IO Error
    IoError(std::io::Error),
    /// EOF while parsing a bracket file.
    EofWhileParsingBracketFile,
    /// Any invalid content while parsing a bracket file.
    InvalidContent(String),
    /// Missing sequence length in the bracket file.
    MissingSequenceLength(std::num::ParseIntError),
    /// Missing Free Energy in the bracket file.
    MissingFreeEnergy(std::num::ParseFloatError),
    /// Nucleotide sequence length does not match the sequence length in the description.
    SequenceLengthMismatch(usize),
    /// Bracket sequence length does not match the sequence length in the description.
    BracketLengthMismatch(usize),
    /// Unmatched closing bracket at index
    UnmatchedClosingBracket(usize),
    /// Unmatched opening bracket at index
    UnmatchedOpeningBracket(usize), 
}

impl std::error::Error for SequenceError {}

impl std::fmt::Display for SequenceError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> fmt::Result {
        match self {
            SequenceError::IoError(io_error) => {
                write!(f, "IO Error reading the Sequence file: {}", io_error)
            }
            SequenceError::InvalidContent(content) => write!(
                f,
                "Invalid content while parsing a bracket file: {}",
                content
            ),
            SequenceError::BracketLengthMismatch(expected_length) => write!(
                f,
                "Bracket sequence length does not match the sequence length of {} in the description",
                expected_length
            ),
            SequenceError::EofWhileParsingBracketFile => {
                f.write_str("EOF while parsing a bracket file.")
            }
            SequenceError::MissingSequenceLength(parse_int_error) => write!(
                f,
                "Missing or non-parsable sequence length of {} in the bracket file.",
                parse_int_error
            ),
            SequenceError::MissingFreeEnergy(parse_float_error) => write!(
                f,
                "Missing or non-parsable Free Energy of {} in the bracket file.",
                parse_float_error
            ),
            SequenceError::SequenceLengthMismatch(expected_length) => write!(
                f,
                "Nucleotide sequence length does not match the sequence length of {} in the description.",
                expected_length
            ),
            SequenceError::UnmatchedClosingBracket(_) => {
                f.write_str("Unmatched closing bracket at index")
            }
            SequenceError::UnmatchedOpeningBracket(_) => {
                f.write_str("Unmatched opening bracket at index")
            }
        }
    }
}

impl From<std::num::ParseIntError> for SequenceError {
    fn from(err: std::num::ParseIntError) -> Self {
        SequenceError::MissingSequenceLength(err)
    }
}

impl From<std::num::ParseFloatError> for SequenceError {
    fn from(err: std::num::ParseFloatError) -> Self {
        SequenceError::MissingFreeEnergy(err)
    }
}

impl From<std::io::Error> for SequenceError {
    fn from(err: std::io::Error) -> Self {
        SequenceError::IoError(err)
    }
}
impl BasePairSequence {
    /// Create and initialise a BasePairSequence
    ///
    /// Creates the default output which is a list of structures with the form:
    /// 1 A 0
    /// 2 C 0
    /// etc where the first row is read as the 1-based index of the Nucleotide "A" in the input string with a matching Nucleotide with index of 0  

    /// Later the code will process the separate bracket strings and replace the third column in this list with the actual index of links to
    /// specific nucleotides in the RNA sequence.
    ///
    pub fn build(rna_sequence: &FASTASequence) -> BasePairSequence {
        let size_of_rnasegment = rna_sequence.nucleotides.len();
        let mut initial_secondary: Vec<NucleotidePair> = Vec::with_capacity(size_of_rnasegment);
        let mut line_number = 1;

        for c in rna_sequence.nucleotides.chars() {
            initial_secondary.push(NucleotidePair::new(line_number, c, 0));
            line_number = line_number + 1;
        }

        let valid_description = Regex::new(r"^>(\S+)\s+CDS=\d+-\d+$").unwrap();
        let capture_name: regex::Captures<'_> = valid_description
            .captures(&rna_sequence.description)
            .unwrap();
        let name = capture_name.get(1).unwrap().as_str();

        BasePairSequence {
            description: name.to_owned(),
            secondary_structure: initial_secondary,
        }
    }

    pub fn generate_connections(&mut self, brackets: &str) -> Result<(), SequenceError> {
        let mut brackets_stack: Vec<char> = Vec::with_capacity(brackets.len());
        let mut pseudoknot_order_1_stack: Vec<char> = Vec::with_capacity(brackets.len());
        let mut pseudoknot_order_2_stack: Vec<char> = Vec::with_capacity(brackets.len());
        let mut pseudoknot_order_3_stack: Vec<char> = Vec::with_capacity(brackets.len());

        let mut index: usize = 0;
        let mut pair_index_stack: Vec<usize> = Vec::with_capacity(brackets.len());

        for c in brackets.chars() {
            match c {
                '.' => (),
                '(' => {
                    brackets_stack.push(c);
                    pair_index_stack.push(self.secondary_structure[index].row_number);
                }
                '[' => {
                    pseudoknot_order_1_stack.push(c);
                    pair_index_stack.push(self.secondary_structure[index].row_number);
                }
                '{' => {
                    pseudoknot_order_2_stack.push(c);
                    pair_index_stack.push(self.secondary_structure[index].row_number);
                }
                '<' => {
                    pseudoknot_order_3_stack.push(c);
                    pair_index_stack.push(self.secondary_structure[index].row_number);
                }
                ')' => match brackets_stack.pop() {
                    Some('(') => match pair_index_stack.pop() {
                        Some(pair_index) => {
                            self.secondary_structure[pair_index].pair_index =
                                self.secondary_structure[index].row_number;
                            self.secondary_structure[index].pair_index =
                                self.secondary_structure[pair_index].row_number;
                        }
                        None => return Err(SequenceError::UnmatchedOpeningBracket(index)),
                    },
                    _ => return Err(SequenceError::UnmatchedClosingBracket(index)),
                },
                ']' => match pseudoknot_order_1_stack.pop() {
                    Some('[') => match pair_index_stack.pop() {
                        Some(pair_index) => {
                            self.secondary_structure[pair_index].pair_index =
                                self.secondary_structure[index].row_number;
                            self.secondary_structure[index].pair_index =
                                self.secondary_structure[pair_index].row_number;
                        }
                        None => return Err(SequenceError::UnmatchedOpeningBracket(index)),
                    },
                    _ => return Err(SequenceError::UnmatchedClosingBracket(index)),
                },
                '}' => match pseudoknot_order_2_stack.pop() {
                    Some('{') => match pair_index_stack.pop() {
                        Some(pair_index) => {
                            self.secondary_structure[pair_index].pair_index =
                                self.secondary_structure[index].row_number;
                            self.secondary_structure[index].pair_index =
                                self.secondary_structure[pair_index].row_number;
                        }
                        None => return Err(SequenceError::UnmatchedOpeningBracket(index)),
                    },
                    _ => return Err(SequenceError::UnmatchedClosingBracket(index)),
                },
                '>' => match pseudoknot_order_3_stack.pop() {
                    Some('<') => match pair_index_stack.pop() {
                        Some(pair_index) => {
                            self.secondary_structure[pair_index].pair_index =
                                self.secondary_structure[index].row_number;
                            self.secondary_structure[index].pair_index =
                                self.secondary_structure[pair_index].row_number;
                        }
                        None => return Err(SequenceError::UnmatchedOpeningBracket(index)),
                    },
                    _ => return Err(SequenceError::UnmatchedClosingBracket(index)),
                },
                _ => panic!("Invalid character {} at index {}", c, index),
            }
            index = index + 1;
        }
        Ok(())
    }
}

impl FASTASequence {
    /// The strings in the Fasta File need to have their contents validated by a set of regexes in the functions below.
    ///
    fn validate_description(description: &str) -> Result<usize, SequenceError> {
        let valid_description = Regex::new(r"^>(\S+)\s+CDS=\d+-(\d+)$").unwrap();
        if valid_description.is_match(description) {
            Ok(valid_description
                .captures(&description)
                .unwrap()
                .get(2)
                .unwrap()
                .as_str()
                .parse::<usize>()?)
        } else {
            Err(SequenceError::InvalidContent(
                "Invalid description".to_owned(),
            ))
        }
    }

    /// Validate the sequence string and return its length
    fn validate_sequence(sequence: &str) -> Result<usize, SequenceError> {
        let valid_sequence = Regex::new(r"^[AaUuGgCc]+$").unwrap();
        if valid_sequence.is_match(sequence) {
            Ok(sequence.len())
        } else {
            Err(SequenceError::InvalidContent("Invalid sequence".to_owned()))
        }
    }

    fn validate_brackets(brackets: &str, expected_length: usize) -> Result<(String, f64), SequenceError> {
        let valid_brackets = Regex::new(r"^[.()]+\s+\([+-]?\d+\.\d+\)$").unwrap();
        let free_energy_chars = Regex::new(r"[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?").unwrap();
        if valid_brackets.is_match(brackets) {
            let mut iter = brackets.split_ascii_whitespace();
            let brackets = iter.next().unwrap();
            if expected_length != brackets.len() {
                return Err(SequenceError::BracketLengthMismatch(expected_length));
            }
            let free = iter.next().unwrap();
            let fe = free_energy_chars
                .captures(free)
                .unwrap()
                .get(0)
                .unwrap()
                .as_str()
                .parse::<f64>()?;
            Ok((brackets.to_string(), fe))
        } else {
            Err(SequenceError::InvalidContent("Invalid brackets".to_owned()))
        }
    }
    /// Create and initialise a FASTASequence from 3 strings that will be validated
    ///
    pub fn build(a: &str, b: &str, c: &str) -> Result<FASTASequence, SequenceError> {
        let desc_length = Self::validate_description(&a)?;
        let nuc_length = Self::validate_sequence(&b)?;
        assert!(
            desc_length == nuc_length,
            "Nucleotide sequence length does not match the sequence length in the description."
        );
        let (brackets, fe) = Self::validate_brackets(&c, desc_length)?;
        assert_eq!(brackets.len(), nuc_length);
        assert!(fe.is_finite(), "Missing Free Energy in the bracket file.");

        Ok(FASTASequence {
            description: a.to_owned(),
            nucleotides: b.to_owned(),
            brackets: brackets.to_string(),
            free_energy: fe,
        })
    }
}

impl std::fmt::Display for FASTASequence {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let re = Regex::new(r"^>(\S+)").unwrap();
        let gene_name = match re.captures(&self.description) {
            Some(caps) => caps.get(1).unwrap().as_str(),
            None => &self.description,
        };
        write!(
            f,
            "Sequence {} has {} nucleotides and {} brackets with a free energy of {}.",
            gene_name,
            self.nucleotides.len(),
            self.brackets.len(),
            self.free_energy
        )
    }
}

impl std::fmt::Display for BasePairSequence {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{} {}\n",
            self.secondary_structure.len(),
            self.description
        )?;
        for pair in &self.secondary_structure {
            write!(
                f,
                "{} {} {} {} {} {}\n",
                pair.row_number,
                pair.nucleotide,
                pair.row_number - 1,
                pair.row_number + 1,
                pair.pair_index,
                pair.row_number
            )?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use regex::Regex;

    #[test]
    fn description_works() {
        let description = ">gene-pgaptmp_000001 CDS=1-66";
        let description_missing_prefix = "gene-pgaptmp_000001 CDS=1-66";
        let description_missing_length = ">gene-pgaptmp_000001 CDS=";
        let description_missing_space = ">gene-pgaptmp_000001CDS=1-66";

        assert_eq!(
            66,
            FASTASequence::validate_description(description).unwrap()
        );
        assert_err!(
            FASTASequence::validate_description(description_missing_prefix),
            Err(SequenceError::InvalidContent(_))
        );
        assert_err!(
            FASTASequence::validate_description(description_missing_length),
            Err(SequenceError::InvalidContent(_))
        );
        assert_err!(
            FASTASequence::validate_description(description_missing_space),
            Err(SequenceError::InvalidContent(_))
        );
    }

    #[test]
    fn sequence_works() {
        let sequence = "augaaacgcauuagcaccaccauuaccaccaccaucaccauuaccacagguaacggugcgggcuga";
        assert_eq!(66, FASTASequence::validate_sequence(sequence).unwrap());
    }

    #[test]
    fn brackets_work() {
        let good_brackets =
            "..........(((((.(((((.(((((.....................))))).)))).).))))) (-14.90)";
        let bad_brackets =
            "..........(((((.(((((.(((((.....................))))).)))).).)))))(-14.90)";

        let (brak, fe) = FASTASequence::validate_brackets(good_brackets, 66).unwrap();

        assert_eq!(
            brak,
            "..........(((((.(((((.(((((.....................))))).)))).).)))))"
        );
        assert_eq!(fe, -14.90);
        assert_err!(
            FASTASequence::validate_brackets(bad_brackets, 66),
            Err(SequenceError::InvalidContent(_))
        )
    }
}
