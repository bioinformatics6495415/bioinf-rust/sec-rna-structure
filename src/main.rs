use std::ffi::OsStr;
/// This is the main.rs file for the sec-rna-structure project.
/// It contains the implementation of a program that converts a Dot Bracket File into a Connectivity Table file.
/// The program takes command-line arguments to specify the input file path and the desired operations.
/// It uses the `clap`, `itertools`, and `regex` crates for argument parsing, sequence processing, and regex matching, respectively.
/// The program defines the `Arguments` struct to hold the command-line arguments and implements the `Parser` trait from `clap` for argument parsing.
/// It also defines the `FASTASequence` and `BasePairSequence` structs for representing the primary nucleotide sequences and secondary structures.
/// The `main` function is the entry point of the program, which reads the input file, processes the sequences, and prints the resulting base pair sequences.
/// The `read_bracket_file` function reads the input file, validates the lines using regex matching, and constructs `FASTASequence` objects.
/// The `detail_information` and `transform_brackets` functions are placeholders for future functionality.
use std::fs::File;
use std::io::{BufRead, BufReader, BufWriter, Write};
use std::path::Path;

use clap::Parser;
use itertools::Itertools;
use regex::Regex;

use sec_rna_structure::BasePairSequence;
use sec_rna_structure::FASTASequence;
use sec_rna_structure::SequenceError;

#[derive(Parser, Default, Debug)]
#[clap(author, version = "0.2.0", about)]
/// Converts a Dot Bracket File into a Connectivity Table file. The dot bracket
/// file should contain 1 or more set of nucleotide sequence and an associated secondary structure
struct Arguments {
    /// The path to the file to read
    path: std::path::PathBuf,
    /// If set, only print information on the brackets file. Default to true
    // #[clap(default_value_t=true, short, long, action=ArgAction::SetTrue)]
    #[clap(default_value_t = true, short, long)]
    information: bool,
    /// If set, convert the brackets file to a connectivity table file. Default to false
    #[clap(default_value_t = false, short, long)]
    // #[clap(default_value_t=false, short, long, action=ArgAction::SetFalse)]
    transform: bool,
}

fn main() {
    let args = Arguments::parse();

    let path_clone = args.path.clone();
    let file_name = Path::new(&path_clone).file_name();
    let output_directory = Path::new(&path_clone).parent().unwrap();

    match File::open(args.path) {
        Ok(file) => {
            let reader = BufReader::new(file);
            match read_bracket_file(reader) {
                Ok(seq) => {
                    if args.information {
                        detail_information(file_name.unwrap(), &seq);
                    }
                    if args.transform {
                        match write_connectivity_table(output_directory, &seq) {
                            Ok(_) => println!("Connectivity Table files have been created."),
                            Err(e) => println!("Error: {}", e),
                        };
                    }
                }
                Err(_) => println!("Cock up"),
            }
        }
        Err(error) => panic!("Problem opening the file: {:?}", error),
    }
}

fn read_bracket_file(reader: BufReader<File>) -> Result<Vec<FASTASequence>, SequenceError> {
    let mut sequences: Vec<FASTASequence> = vec![];

    for chunk in &reader.lines().chunks(3) {
        let chunk = chunk.collect::<Result<Vec<_>, _>>()?;
        if chunk.len() == 3 {
            let line1 = &chunk[0];
            let line2 = &chunk[1];
            let line3 = &chunk[2];

            match FASTASequence::build(line1, line2, line3) {
                Ok(seq) => sequences.push(seq),
                Err(e) => println!("Error: {}", e),
            }
        } else {
            return Err(SequenceError::EofWhileParsingBracketFile);
        }
    }
    Ok(sequences)
}

/// Print out the details of the sequences in the file
fn detail_information(file_name: &OsStr, sequences: &Vec<FASTASequence>) {
    println!(
        "The file {:?} contains {} sequences",
        file_name,
        sequences.len()
    );

    for seq in sequences {
        println!("{}", seq);
    }
}

/// Generate the connectivity table for each Fasta Sequence and write it to a distinct file in the output directory
fn write_connectivity_table(
    directory: &Path,
    sequences: &Vec<FASTASequence>,
) -> Result<(), Box<dyn std::error::Error>> {
    for seq in sequences {
        let mut base_pair_sequence = BasePairSequence::build(seq);
        match base_pair_sequence.generate_connections(&seq.brackets) {
            Ok(_) => {
                let file_name = format!("sequence_{}.ct", base_pair_sequence.description);
                let file_path = directory.join(file_name);
                let file = File::create(file_path)?;
                let mut writer = BufWriter::new(&file);
                write!(writer, "{}", base_pair_sequence)?;
            }
            Err(e) => println!("Error: {}", e),
        };
    }

    Ok(())
}
