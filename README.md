# Secondary Structure

## Name
sec-rna-structure - Generate the Secondary Structure for RNA Sequences

## Description
TThe first release of this program converts a Dot Bracket File into a Connectivity Table file. The dot bracket file contains 1 or more set of nucleotide sequence and an associated secondary structure:

### Dot Bracket File
The Dot Bracket file is expected to follow the following format:

  The first line is a title and starts with a ">" character.
  The second line contains the nucleotide sequence.
  The third line contains secondary structure information in dot-bracket notation:
    The dot/period "." represents an unpaired nucleotide.
    An open-parenthesis "(" represents the 5'-nucleotide in a pair, and the matching closing parenthesis ")" represents the 3'-nucleotide in the pair.
    Other "bracket"-type symbols can be used to represent basepairs, thereby allowing pseudo-knots to be encoded. The Bracket Characters processed by this version of the program are:
      () to represent a base-pair 
      [] to represent an order-1 pseudoknot pairing
      {} to represent an order-2 pseudoknot pairing
      <> to represent an order-3 pseudoknot pairing

### Connectivity Table File
The CT file will follow the following format:

  Start of first line: number of bases in the sequence
  End of first line: title of the structure
  Each of the following lines provides information about a given base in the sequence. Each base has its own line, with these 6 elements in order:
    Base number: index n starting at '1'
    Base (A, C, G, T, U, X)
    Index n-1
    Index n+1
    Number of the base to which n is paired. No pairing is indicated by 0 (zero).
    Natural numbering: repeat of index n

See - [ ] [File Formats](https://rna.urmc.rochester.edu/Text/File_Formats.html) for further details.

The real purpose here is to learn rust with some straightforward algorithms and file and error handling.


## Usage
Usage: sec-rna-structure [OPTIONS] <PATH>

Arguments:
  <PATH>  The path to the file to read

Options:
  -i, --information  If set, only print information on the brackets file
  -t, --transform    If set, convert the brackets file to a connectivity table file
  -h, --help         Print help
  -V, --version      Print version

## Contributing
Fully open tocontributions - knock yourself out!

## Authors and acknowledgment
Eoin Leahy is the Author

## License
This project is licensed according to the MIT Open Source license.

## Project status
First stab at a rust project, will probably use this as a project for adding functionality as I learn more.
